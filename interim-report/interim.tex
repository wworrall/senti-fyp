\documentclass[10pt]{article}

% First load extension packages
\usepackage[a4paper,margin=25mm]{geometry}    % page layout
\usepackage{setspace} \onehalfspacing         % line spacing
\usepackage{amsfonts,amssymb,amsmath}         % useful math extensions
\usepackage{graphicx}                         % graphics import
\usepackage{siunitx}                          % easy SI units
\usepackage{hyperref}                         % link references
\usepackage{subcaption}                       % figure subcaptions
\usepackage{paralist}                         % tighter lists
\usepackage{pgfgantt}                         % gantt charts

\usepackage{titlesec} % shrink space around section headings
\titlespacing\section{0pt}{0pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsection{0pt}{0pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}
\titlespacing\subsubsection{0pt}{0pt plus 4pt minus 2pt}{0pt plus 2pt minus 2pt}

\usepackage{titling} % raise title section up
\setlength{\droptitle}{-2.5cm}

% Change paragraph indentation
\setlength{\parskip}{8pt}
\setlength{\parindent}{0pt}

% Tighten references
\usepackage{natbib} \setlength{\bibsep}{0pt}

% User-defined commands
\newcommand{\diff}[2]{\frac{\mathrm{d}{#1}}{\mathrm{d}{#2}}}
\newcommand{\ddiff}[2]{\frac{\mathrm{d}^2{#1}}{\mathrm{d}{#2}^2}}
\newcommand{\pdiff}[2]{\frac{\partial{#1}}{\partial{#2}}}
\newcommand{\pddiff}[2]{\frac{\partial^2{#1}}{\partial{#2}^2}}
\newcommand{\pdiffdiff}[3]{\frac{\partial^2{#1}}{\partial{#2}\partial{#3}}}
\renewcommand{\vec}[1]{\boldsymbol{#1}}
\newcommand{\Idx}{\;\mathrm{d}x}
\newcommand{\Real}{\mathbb{R}}
\newcommand{\Complex}{\mathbb{C}}
\newcommand{\Rational}{\mathbb{Q}}
\newcommand{\Integer}{\mathbb{Z}}
\newcommand{\Natural}{\mathbb{N}}

% topmatter
\title{Improvement of current sentiment analysis utilising Google's SyntaxNet}

\author{William Worrall \\ Supervised by Prof. Jonathan Lawry}

\date{\today}

% main body
\begin{document}
\maketitle

\section{Introduction}

Sentiment is a view or opinion that is expressed. This may be regarding an
object, a movie, a person or an organisation. Whilst libraries exist that can
give a numerical sentiment associated with individual words, computers have
difficulty understanding views and opinions expressed in natural language. As
the amount of natural language data grows rapidly, it is becoming ever more
crucial that computers can predict associated sentiments in a way a human might
associate sentiment with a phrase.  With the ability to extract sentiment from
pieces of natural text, comes the ability to better control computers with
natural language, or harvest sentiment from sources of natural language data.

Sentiment can typically be expressed using a numeric scale with low values and
high values reflecting negative and positive opinions, respectively. However,
for a given sample of natural language text, assignment of a sentiment value
that is consistent with a humans interpretation is often made difficult due to a
complicated composition of terms.

An advanced sentiment analysis tool which can capture the complex composition
of phrases has both commercial value and value to society. Some applications of
this tool are:
\begin{compactitem}
    \item \textbf{Opinion aggregation services.} Not necessarily
        restricted to opinions about products; an advanced sentiment analysis tool
        would be useful in aggregating opinions on political figures, policies
        and events which are of concern to many people. This aggregation
        could be done in real-time (eg. analysing live Twitter streams) or over
        pre-existing data.
    \item \textbf{Assistance to other technology systems.} An email filter may
        benefit from being able to detect overt or angry language, which is
        perhaps considered inappropriate in some organisations. In displaying
        online adverts, it may be beneficial to detect for sensitive content
        which would make the placement of a particular advertisement
        inappropriate.
    \item \textbf{Applications in business and government intelligence.} In
        filtering large amounts of textual data, an organisation wishing to gain
        intelligence may scan the data for keywords. However, being able to
        detect the context and opinions regarding these keywords would likely
        increase accuracy of detecting data which is of most interest.
    \item \textbf{Applications in customer interfacing.} Automated messaging
        customer assistants are becoming increasingly popular as an initial
        customer interface. Their benefits in cutting costs are undisputed. By
        detecting for keywords in a customer's message, they can often remedy
        common issues.  However, if a customer isn't satisfied by the automated
        assistant, the assistant needs to be able to detect this and take the
        appropriate action as not to damage the customer's opinion of the
        service.
\end{compactitem}

Many approaches to analysing pieces of text for sentiment use a bag of words
model \cite{Pang+Lee:08b} and consider the occurrences of words which have a
particularly strong sentiment associated with them. However, a bag of words
model ignores the ordering of words and therefore also ignores any structural
features and composition of the data. A simple example is how the presence of
a negation cannot be detected using a bag of words model. TextBlob, a Python
library for processing textual data \cite{TextBlob}, includes a bag-of-words
based sentiment tool. The sentence \emph{`This movie was actually neither that
funny, nor super witty.'} is misclassified by TextBlob, which gives scores of
0.27 and 0.69 for polarity and subjectivity, respectively. Here a positive
polarity score represents positive sentiment.  Misclassification is due to the
strong positive sentiment expressed by \emph{`funny'} and \emph{`witty'} and
the inability to detect the preceding negation.

More advanced approaches have been taken to sentiment analysis which are
discussed in detail in the review of previous work. The state of the art at the
time of writing is the open sourced model by Socher et al. of the Natural
Language Processing department, Stanford University \cite{Socher-etal:2013}.
Using a specially constructed sentiment treebank, their own natural language
parser and a recursive neural tensor network (RNTN) they achieve a fine-grained
sentiment classification accuracy of 80.7\% which is an improvement of 9.7\%
over bag of features baselines.

In a May 2016 press release, Google announced an open source syntactic parser
named SyntaxNet \cite{DBLP:journals/corr/AndorAWSPGPC16}. It is currently the
state of the art in dependency parsing and part of speech (POS) tagging. As a
key first component in many natural language processing systems, including
Stanford's sentiment analysis model, it is likely that such an advanced
syntactic parser could improve the performance of many existing natural language
tools. As initial work, this project aims plug in Google's pre-trained English
language instance of SyntaxNet, `Parsey McParseface' (PMPF), to Stanford's RNTN
model. It is hoped that by using an improved syntactic parser, we can improve on
the fine-grained sentiment analysis results achieved by Socher et al.. Finally this
work will look to improve further on the RNTN model by replicating the model
but also considering word-wise sentiment values during training. This
interim report discusses the discoveries and progress made so far.

\section{Aims and Objectives}
The aim of this work is to extend Socher et al.'s RNTN model by incorporating
PMPF and introducing word-wise sentiment ground truths.

The objectives by which we hope to achieve this aim are as follows:
\begin{compactitem}
    \item Produce a wrapper for PMPF such that it can be used in place of the
        Stanford Parser in a black-box manner.
    \item Retrain Socher et al.'s model with PMPF as used to produce the
        dependency trees required as input.
    \item Use test data to establish results and effectiveness of PMPF in the
        model
    \item Produce a wrapper for SentiWordNet which can provide sentiment scores
        for the appropriate definition of the word given a word string and a POS
        tag.
    \item Integrate the word level sentiment scores in to the training scheme of
        the RNTN model
    \item Use test data to establish the results and effectiveness of
        training the model using word-wise sentiment ground truths.
\end{compactitem}


\section{Review of Previous Work}

There has been a lot of previous work carried out in the field of sentiment
analysis. Socher et al. presents the Recursive Neural Tensor Network model which
is the current state of the art and achieves 80.7\% in prediction of
fine-grained sentiment labels. This is a vast improvement on the sub 60\%
prediction accuracy seen using bag of words classifiers on short texts such as
Twitter data \cite{Wang:2012:SRT:2390470.2390490}. This section will discuss
this previous work in more detail and also cover related previous work which has
driven this field to its current state of progress.

\textbf{Bag of Words Models}\\
Preprocessing a document in to a bag of words representation gives a vocabulary
of words which are contained in the document and corresponding frequency values.
All information about the original document structure and order of words is
lost. Using a word-wise sentiment library such as SentiWordNet by Natural
Language Toolkit (NLTK), we can get sentiment scores for each of the words in
the vocabulary. Techniques can be applied to disregard words which express no
subjectivity, and we can consider words which express strong sentiment bias, as
well as the frequency of these words to assign a sentiment label to the overall
document.

This technique can work well with texts which contain enough occurrences of
highly expressive words such as \emph{`excellent'} or \emph{`terrible'}, but can
struggle to correctly classify shorter texts which will have far fewer signals
per document. Twitter data is a typical example on which this technique may
fail.

Wang et al. \cite{Wang:2012:SRT:2390470.2390490} carried out real-time analysis
of Tweets regarding the 2012 US Presidential Candidates. Their classifier uses
unigram features with an attempt to preserve punctuation and emoticons which may
signify sentiment, such as exclamation points. This is equates to a
simple bag of words model with some punctuation and emoticons included in the
vocabulary. They achieved three-class classification accuracy of 59\% when
including the neutral class, which narrowly exceeds the baseline of classifying
all data as negative, the most prevalent sentiment category (56\%) in their
training data. Wang et al. explain that Tweets are often highly opinionated but
are always short and often contain sarcastic terms which further complicates the
task of classifying their sentiment. Being able to consider word cooccurrences,
and general composition of the text would perhaps enable better classification
accuracy over a bag of words model.

\textbf{Logical Form} \\
This related field deals with trying to to map sentences to a logical form, that
is mapping sentences to a lambda-calculus encoding of their semantics.
Zettlemoyer and Collins \cite{zettlemoyer2005learning} present work on this.
However, it is proven only to work well on closed domains and due to the diverse
nature of compositions in natural language, a more robust and flexible solution
is required to address the sentiment analysis of natural language text.

\textbf{Semantic Vector Spaces} \\
Discussed by Turney and Pantel \cite{DBLP:journals/corr/abs-1003-1141}, is the
concept of representing words, or phrases as points in a vector space.  Points
that are close together in this space are semantically similar and points that
are far apart are semantically distant.  Semantic vector spaces can be
constructed by extracting knowledge from a corpus in an unsupervised manner.
Whilst such a technique is useful for learning similarities and dependency
statistics between words or phrases, to capture full compositionality in phrases
requires high dimensionality.

\textbf{Compositionality in vector Spaces}\\
Explored by Yessenalina and Cardie \cite{Yessenalina:2011:CMM:2145432.2145452}
uses matrix representations of words and calculates compositional, phrase-level
sentiment. The sentiment is calculated by iterative matrix multiplication.
However, they discuss that learning these models is not trivial since it is a
non convex optimisation and requires a good starting point. Whilst they witness
results which outperform those found using bag of words models, they identify
that using the output from a dependency parser to give the order of composition
instead of using the original order of words would be an avenue to investigate.

\textbf{Recursive Neural Tensor Network}\\
Work by Socher et al. \cite{Socher-etal:2013} of Stanford University
presents the state of the art in compositional sentiment analysis. They build on
their previous work (Socher et al., 2011a) which uses recursive neural networks
(RNNs) to present a more advanced solution which uses a recursive neural tensor
network (RNTN). This RNTN model is the foundation for the proposed work in this
project which will be discussed later.

Socher et al. have developed the only model that can accurately capture the
effects of negation and it's scope at various tree levels for both negative and
positive phrases. Figure \ref{fig:stanford-demo-tree} shows how positive
sentiment is reversed as the algorithm recursively moves up the parse tree, to
correctly classify the sentence as having negative sentiment.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{./figures/stanford-demo-tree}
    \caption{Recursive sentiment classification of a parse tree using Stanford's
sentiment classifier. It can be seen that the negations propagate an
expected switch in sentiment as we move up the tree. This sentence is
misclassified by TextBlob's sentiment analysis tool. Figure generated online at
\protect\url{http://nlp.stanford.edu:8080/sentiment/rntnDemo.html} [accessed
17-November-2016].}
    \label{fig:stanford-demo-tree}
\end{figure}

Fundamental to the success of their model was the construction of a sentiment
treebank. They argue that there was several datasets available with document
wise or chunk wise sentiment, but in order to predict sentiment for short texts
with typically less overall signal, such as Twitter data, fine-grain sentiment
labels were required on shorter phrases. The sentiment treebank is the first to
provide fine-grained sentiment labels for every syntactically plausible phrase
in thousands of sentences. The sentences they use are from the corpus of movie
reviews from the \url{rottentomatoes.com} website, originally collected and
published by Pang and Lee (2005) \cite{Pang+Lee:05a}. The syntactically
plausible set of 215,154 phrases was derived from the 10,662 sentences using
the Stanford Parser (Klein and Manning, 2003)
\cite{Klein:2003:AUP:1075096.1075150}, the phrases were then assigned fine
grained sentiment labels using Amazon Mechanical Turk, using the interface shown
in figure \ref{fig:amt-labelling-interface}.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{./figures/amt-labelling-interface}
    \caption{The labelling interface used in Amazon Mechanical Turk. Random
        phrases were shown and the annotators had a slider to select the
        sentiment and its degree. Figure sourced from Socher et al. (2013)
        \cite{Socher-etal:2013}}
    \label{fig:amt-labelling-interface}
\end{figure}

The principle idea is to use the same tensor-based composition function for all
nodes in the parse tree. This gives a model which takes a fixed number of
parameters as an input, namely a parse tree and $d$-dimensional word vectors.
Whilst a standard RNN would be suitable, it was replaced with a RNTN since in an
RNN the input vectors only implicitly interact through a nonlinearity
(squashing) function, whereas the RNTN developed allowed for direct,
multiplicative interactions.

Socher et al. test their model quantitatively and they test it qualitatively on
two linguistic phenomena. Testing the accuracy in predicting fine-grained
sentiment labels for the test data on all phrases, and binary sentence labels
for all sentences, they found the RNTN to outperform the RNN discussed earlier
and other models such as a bag of words model.  They observe that the bag of
words model works better on longer phrases, as expected. Testing for contrastive
conjunctions which are sentences with an \emph{`X but Y'} structure, they find
for the 131 applicable sentences, the RNTN outperforms the RNN with a success of
41\% over the 36\% given by RNN. Finally, they tested for high level negations
in sentences in two ways; negating positive sentences and negating negative
sentences. With the former they found a success of 71.4\% on test data compared
to a much lower score of 33.3\% for the RNN model. With the latter they found a
success of 81.8\% compared with 45.5\% for the RNN model.

Whilst these tests prove a significant step forward in compositional sentiment
analysis, we aim to further improve these results by utilising a new parser
and including sentiment ground truths in the input word vectors of the RNTN
model.


\section{Project plan}

In this section, we shall define the proposed work to achieve the aim of
improving the compositional sentiment analysis of Socher et al.'s
state of the art model.

\subsection{Introducing an improved parser}

Socher et al.'s RNTN model takes a parse tree and learned word vectors as input.
In their model the parse tree input is generated by the Stanford Parser. The
parser produces a dependency tree and labels each token in the sentence with a
part of speech (POS) tag. The parser is therefore a key component to the
functionality of their model. SyntaxNet is a open-source neural network
framework implemented in TensorFlow. It is designed to provide a foundation for
natural language understanding systems, by performing POS tagging, dependency
parsing and sentence compression. With the May 2016 press release
\cite{SyntaxNet-press-release}, Google also announced a pre-trained, state-of-the
art English dependency parser and POS tagger called `Parsey McParseface'
(PMPF). It is the POS tagging and dependency parsing functionality which we are
concerned with in this work, and for dependency parsing on the Wall Street Journal,
PMPF achieved the best-ever published unlabelled attachment
score of 94.61\%. An example parse tree can be seen illustrated in figure
\ref{fig:pmpf-illustrated} and the equivalent textual parse tree output (as seen
when running PMPF as a command-line application) can be seen in figure
\ref{fig:pmpf-text}.

\begin{figure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=0.85\textwidth]{./figures/pmpf-dependency-tree}
        \subcaption{An illustration of a parse tree generated by Parsey
        McParseface}\label{fig:pmpf-illustrated}
    \end{subfigure}
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=0.65\textwidth]{./figures/pmpf-dependency-tree-text}
        \subcaption{Textual output of a parse tree generated by Parsey
        McParseface (as seen when running PMPF as a command-line application). POS
        tags and dependency tags follow each word in the
        output.}\label{fig:pmpf-text}
    \end{subfigure}
    \caption{Parsey McParseface: A trained instance of SyntaxNet capable of
    parsing English text.}\label{fig:pmpf}
\end{figure}

The dependency parser constitutes a key part of the functionality and success of
of Socher et al.'s RNTN model. Therefore as a first line of work, this project
will aim to substitute the Stanford Parser for PMPF. Deploying this new parser
with the model is predicted to correctly label sentiment in cases where it is
previously wrong due to the Stanford Parser not giving an accurate dependency
tree. However, to achieve the greatest improvements in performance, it is
proposed that we will retrain Socher et al.'s model with PMPF to fully utilise
the advantage of the new state of the art parser and improve the RNTN model.


\subsection{Sentiment ground truths}

In training Socher et al.'s sentiment model, they aim to learn word-wise vectors
which then act as ground truths and are used as weights to recursively calculate
sentiment in a bottom up fashion through the parse tree. This concept is
illustrated on an example trigram in figure \ref{fig:socher-mod-normal}. In
training, these vectors are initialised by randomly sampling from a uniform
distribution $U(-r,r)$, where $r = 0.0001$. During training these vectors are
learned by using the sentiment labels of the phrases in the training data.

\begin{figure}[h!]
    \centering
    \subcaptionbox{Socher et al.'s model uses arbitrary length word vectors
        which are randomly initialised and then their values
        learned.\label{fig:socher-mod-normal}}
        {\includegraphics[width=7.5cm]{./figures/socher-mod-normal}}
    \hspace{0.5cm}
    \subcaptionbox{As a modification to Socher et al.'s model, we shall
        incorporate word-wise sentiment scores as ground truths in learning the
        rest of the values in the word vector.\label{fig:socher-mod-sent}}
        {\includegraphics[width=7.5cm]{./figures/socher-mod-sent}}
    \caption{A schematic which illustrates how probabilities of sentiment
        classification (\textbf{p}) are recursively calculated for each node
        moving up the parse tree for an example trigram.\label{fig:socher-mod}}
\end{figure}

We will aim to introduce word-level sentiment as a ground
truth in to the training scheme.  NLTK's SentiwordNet is a word-wise sentiment
library implemented as a Python module. Given a word input it will return a
`synset' with all known definitions of the word and for each definition a
triplet of values; positive and negative sentiments, and an objective score.
The triplet will always sum to 1.  An example synset for the input word `good'
is shown in figure \ref{fig:swn-good}.

\begin{figure}
    \centering
    \includegraphics[width=0.75\textwidth]{./figures/swn-good}
    \caption{A synset for the word `good' when input in to SentiWordNet.
    Definitions are shown in popularity of usage order. The letter `a' shows
that it is an adjective and the number `01' shows that it is the most common
definition. The positive and negative sentiment, and objective scores for
`good.a.01' are printed below the synset output. These three scores sum to 1 for
every word.}
    \label{fig:swn-good}
\end{figure}

From the POS tags given by PMPF, we can identify the intended definition of each
word in the parse tree and get the appropriate sentiment scores from the
SentiWordNet synset for that word. It is proposed to combine word level
sentiment scores with the input word vectors which are learned during training
of Socher et al.'s model. This is illustrated in figure \ref{fig:socher-mod}
which shows arbitrary-length input word vectors (figure
\ref{fig:socher-mod-normal}) and these arbitrary-length word vectors combined
with sentiment vectors (figure \ref{fig:socher-mod-sent}). The sentiment part of
the word vector will be used as a ground truth to aid in learning the other
values.

\subsection{Timeline of work}

The work outlined above and in the Aims and Objectives section will be completed
to the schedule which is detailed in the Gantt chart seen in figure
\ref{fig:gantt-chart}. The following bullet points expand on each of the tasks
which are scheduled in the Gantt chart.

\begin{compactitem}
    \item \textbf{PMPF \& SentiWordNet Wrappers.} Build two wrappers; a wrapper so
        that PMPF can be plugged in in the place of the Stanford Parser in
        Socher et al.'s RNTN model, and a wrapper for SentiWordNet which
        will take a word string and POS tag as input and return sentiment values
        for the most likely intended definition of the word.
    \item \textbf{PMPF.} Retrain the RNTN model with PMPF in place
        of the Stanford Parser.
    \item \textbf{Word-wise sentiment.} Retrain the RNTN model and integrate
        word-wise sentiments from SentiWordNet as ground truths.
    \item \textbf{Word-wise sentiment \& PMPF.} Retrain the RNTN model combining
        word-wise sentiments as ground truths and PMPF in place of the
        Stanford Parser.
    \item \textbf{Write up.} Finalise results and figures and complete write up.
\end{compactitem}

\begin{figure}
\centering
\begin{ganttchart}[vgrid,hgrid, x unit=0.4cm, y unit chart=0.5cm, bar/.append
    style={fill=blue, rounded corners=2pt},]{1}{24}
\gantttitle{2016}{8} \gantttitle{2017}{16}\\
\gantttitlelist{"Nov","Dec","Jan","Feb","Mar","Apr"}{4}\\
\ganttgroup{Teaching block 1}{1}{8} \\
\ganttbar{PMPF \& SentiWordNet Wrappers.}{4}{6} \\
\ganttgroup{Teaching block 2}{9}{24}\\
\ganttbar{PMPF}{7}{12} \\
\ganttbar{Word-wise sentiment}{13}{16} \\
\ganttbar{Word-wise sentiment \& PMPF}{17}{18} \\
\ganttbar{Write up}{19}{21}
\end{ganttchart}
\caption{A Gantt chart showing the schedule of work for this project.}
\label{fig:gantt-chart}
\end{figure}


\section{Progress}

The progress made so far includes:

\begin{compactitem}
    \item \textbf{Background research.} Including establishing the current state of the
        art in compositional sentiment analysis and a review of previous work
        seen in this report.
    \item \textbf{Resourced appropriate data.} To enable retraining of the RNTN
        model and allow accurate comparisons, the sentiment treebank produced by
        Socher et al. has been sourced and downloaded.
    \item \textbf{Software installation.} SentiWordNet and SyntaxNet have been
        sourced and installed on a Linux development machine. In particular,
        SyntaxNet has several dependencies which each needed to be carefully
        installed, making installation more consuming than anticipated.
    \item \textbf{Software operation.} Established how to operate PMPF and
        Socher et al.'s model as command-line applications.
\end{compactitem}

\bibliographystyle{plain}
{\small
\bibliography{refs}
}

\end{document}
