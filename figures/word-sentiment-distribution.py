import sys
import matplotlib.pyplot as plt

import figutils as fu
from sentimodels import datautils as du
from sentimodels import sentiutils as su


def make_fig(fig_name=None):
    train, test, dev = du.load_data()

    # concatenate all sentences in sst
    all_sentences = []
    for d in train:
        all_sentences.append(d['sen_str'])
    for d in test:
        all_sentences.append(d['sen_str'])
    for d in dev:
        all_sentences.append(d['sen_str'])

    vocab = [] # all words/tokens without repeats

    for s in all_sentences:
        tokens = s.split()
        for t in tokens:
            if t.lower() not in vocab:
                vocab.append(t.lower())

    sentiments_average = [] # use 'average' wordwise sentiment method
    sentiments_popular = [] # use 'popular' wordwise sentiment method

    for word in vocab:
        sent_av = su.swn_sentiment(word, "AVERAGE")
        if sent_av is not None:
            sentiments_average.append(sent_av)
        sent_pop = su.swn_sentiment(word, "POPULAR")
        if sent_pop is not None:
            sentiments_popular.append(sent_pop)


    fig = plt.figure(figsize=(6,6))
    av_ax = fig.add_axes([0.15,0.60,0.8,0.37]) # top
    pop_ax  = fig.add_axes([0.15,0.1,0.8,0.37]) # middle

    av_ax.hist(sentiments_average, bins=10, ec="black", range=[0,1])
    av_ax.set_ylim([0,12500])
    av_ax.set_xlabel('Word-wise sentiment dist. using Average method',
            size=14)
    av_ax.set_ylabel('Word count', size=14)
    av_ax.tick_params(axis='both', which='major', labelsize=12)

    pop_ax.hist(sentiments_popular, bins=10, ec="black", range=[0,1])
    pop_ax.set_ylim([0,12500])
    pop_ax.set_xlabel('Word-wise sentiment dist. using Popular method',
            size=14)
    pop_ax.set_ylabel('Word count', size=14)
    pop_ax.tick_params(axis='both', which='major', labelsize=12)

    fu.show_or_save(fig, fig_name)


if __name__ == "__main__":
    prog_name = sys.argv[0]
    args = sys.argv[1:]
    
    if len(args) > 0 and args[0] == "--save":
        fig_name = prog_name.split('.')[0]
    else:
        fig_name = None

    make_fig(fig_name)
