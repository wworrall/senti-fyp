import sys

import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize

import figutils as fu
from sentimodels import bagofwords as bow
from sentimodels import baselines as bl


def make_plot(senti_method, fig_prefix):

    model_name = "Simple BOW model"

    print "{} with setiment method: {}".format(model_name, senti_method)

    # run the model
    Y_scores, Y_labels, Y_bins, X_scores, X_labels, X_bins = \
            bow.bag_of_words(senti_method)

    fu.print_model_accuracy(Y_scores, Y_labels, Y_bins, X_scores, X_labels,
            X_bins, senti_method)

    fu.make_scatter_plot(Y_scores, X_scores, model_name, senti_method,
            fig_prefix)

    fu.make_confmat_plot(Y_scores, Y_labels, X_scores, X_labels, senti_method,
            fig_prefix)


if __name__=="__main__":

    prog_name = sys.argv[0]
    args = sys.argv[1:]

    usage_text = """usage: {} senti_method [--save]
    senti_method: the sentiment method to use - "AVERAGE" or "POLAR"
    [--save]    : save output to figure file."""

    if len(args) < 1:
        print usage_text.format(prog_name)
        sys.exit(1)

    senti_method = args[0]

    if len(args) > 1 and args[1] == "--save":
        fig_prefix = prog_name.split('.')[0]
        fig_prefix += "-" + senti_method.lower()
    else:
        fig_prefix = None

    make_plot(senti_method, fig_prefix)
    print ""
