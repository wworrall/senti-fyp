import sys

import matplotlib.pyplot as plt

import figutils as fu
from sentimodels import datautils as du


def make_fig(fig_name=None):
    train, test, dev = du.load_data()
    train_sents = [ d["senti_score"] for d in train ]
    test_sents = [ d["senti_score"] for d in test ]
    dev_sents = [ d["senti_score"] for d in dev ]

    fig = plt.figure(figsize=(6,9))
    train_ax = fig.add_axes([0.15,0.76,0.8,0.23]) # top
    test_ax  = fig.add_axes([0.15,0.42,0.8,0.23]) # middle
    dev_ax   = fig.add_axes([0.15,0.08,0.8,0.23]) # bottom

    train_ax.hist(train_sents, bins=10, ec="black")
    train_ax.set_xlabel("Training Data Sentiment Distribution", size=14)
    train_ax.set_ylabel("Word Count", size=14)
    train_ax.tick_params(axis='both', which='major', labelsize=12)

    test_ax.hist(test_sents, bins=10, ec="black")
    test_ax.set_xlabel("Testing Data Sentiment Distribution", size=14)
    test_ax.set_ylabel("Word Count", size=14)
    test_ax.tick_params(axis='both', which='major', labelsize=12)

    dev_ax.hist(dev_sents, bins=10, ec="black")
    dev_ax.set_xlabel("Development Data Sentiment Distribution", size=14)
    dev_ax.set_ylabel("Word Count", size=14)
    dev_ax.tick_params(axis='both', which='major', labelsize=12)

    fu.show_or_save(fig, fig_name)


if __name__ == "__main__":
    prog_name = sys.argv[0]
    args = sys.argv[1:]
    
    if len(args) and args[0] == "--save":
        fig_name = prog_name.split('.')[0]
    else:
        fig_name = None

    make_fig(fig_name)
