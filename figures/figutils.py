import os
import sys
import numpy as np
import matplotlib.pyplot as plt

from sentimodels import baselines as bl
from sklearn.preprocessing import normalize


def print_model_accuracy(Y_scores, Y_labels, Y_bins, X_scores, X_labels,
        X_bins, senti_method):

    # get baseline mean-squared error and calculate our mean-squared error
    ur_bl_error = bl.ur_err_test_data()
    print "Baseline error guessing uniformly at random: {0:.6f}".format(ur_bl_error)
    med_bl_error = bl.med_err_test_data()
    print "Baseline error guessing median sentimennt (0.5): {0:.6f}".format(med_bl_error)
    err = Y_scores - X_scores
    err2 = np.square(err)
    mean_err2 = np.divide(np.sum(err2), len(Y_scores))
    print "Model mean-squared error: {0:.6f}".format(mean_err2)

    # calculate % missclassification on fine grained sentiment labels
    corr_lab = incorr_lab = 0
    for idx, lab in enumerate(Y_labels):
        if lab == X_labels[idx]:
            corr_lab+=1
        else:
            incorr_lab+=1
    class_acc = (float(corr_lab) / len(Y_labels)) * 100 # % missclassified
    print "Fine grained sentiment classification accuracy: \
{0:.2f}%".format(class_acc)

    # calculate % missclassification on binary sentiment labels
    corr_lab = incorr_lab = 0
    for idx, lab in enumerate(Y_bins):
        if lab == X_bins[idx]:
            corr_lab+=1
        else:
            incorr_lab+=1
    class_acc = (float(corr_lab) / len(Y_bins)) * 100 # % missclassified
    print "Binary sentiment classification accuracy: \
{0:.2f}%".format(class_acc)


def make_scatter_plot(Y_scores, X_scores, model_name, senti_method, fig_prefix):
    # get a line of best fit:
    m, b = np.polyfit(np.reshape(X_scores,-1), np.reshape(Y_scores,-1), 1)

    fig = plt.figure(figsize=(6,6))
    ax = fig.add_axes([0.1,0.1,0.8,0.8])
    ax.scatter(X_scores, Y_scores, s=10, c="red", label="Sentiment Prediction")
    ax.plot([0, 1], [0 , 1], 'k-', label="y = x") # y = x
    plt.plot(np.linspace(0,1,11), m*np.linspace(0,1,11) + b, color='blue', linestyle='--',
            label="LoBF (gradient = {0:.3f})".format(m))
    ax.set_xlabel("SST sentiment scores", size=14)
    ax.set_ylabel("{} sentiment scores".format(model_name), size=14)
    ax.set_xlim([0,1])
    ax.set_ylim([0,1])
    ax.legend(loc=2)

    if fig_prefix is not None:
        fig_name = fig_prefix + ".scatter"
    else:
        fig_name = None

    show_or_save(fig, fig_name)


def make_confmat_plot(Y_scores, Y_labels, X_scores, X_labels, senti_method,
        fig_prefix):

    # make a confusion matrix to show fine grained classification errors
    ind_map = {"--": 0, "-": 1, "0": 2, "+": 3, "++": 4 } # index map

    conf_mat = np.zeros((5,5)) # 5 x 5 array
    # loop through the actual labels (X_labels) and allocate to the confustion
    # matrix the predicted label
    for i, X_label in enumerate(X_labels):
        row_ind = ind_map[X_label]
        col_ind = ind_map[Y_labels[i]]
        conf_mat[row_ind][col_ind] += 1

    conf_mat_norm = normalize(conf_mat, norm='l1', axis=1)

    fig = plt.figure(figsize=(6,6))
    ax = fig.add_axes([0.15,0.1,0.8,0.8])
    ax.set_aspect(1)
    res = ax.imshow(np.array(conf_mat_norm), cmap=plt.cm.jet, vmin=0, vmax=1,
            interpolation='nearest')

    width, height = conf_mat.shape
    for x in xrange(width):
	for y in xrange(height):
	    ax.annotate(str(int(conf_mat[x][y])), xy=(y, x),
                    horizontalalignment='center',
                    verticalalignment='center',
                    size=12)
    cb = fig.colorbar(res)
    ax.xaxis.tick_top()
    plt.xticks(range(width), ['--', '-', '0', '+', '++'])
    plt.yticks(range(height), ['--', '-', '0', '+', '++'])
    plt.tick_params(axis='both', which='major', labelsize=12)
    ax.set_xlabel('Predicted Classes', size=14)
    ax.xaxis.set_label_position('top')
    ax.set_ylabel('Actual Classes', size=14)

    if fig_prefix is not None:
        fig_name = fig_prefix + ".confmat"
    else:
        fig_name = None

    show_or_save(fig, fig_name)


def show_or_save(fig, fig_name):
    if fig_name is None:
        plt.show()
    else:
        filename = os.path.join('images', fig_name + '.pdf')
        fig.savefig(filename)
