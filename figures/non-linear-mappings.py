import sys
import numpy as np
import matplotlib.pyplot as plt

import figutils as fu


def make_fig(fig_name=None):
    """plot the non-linear sentiment mapping functions square, log, cos as
described in the report."""

    x = np.linspace(0,1,101)
    y = x

    square = np.square(x)
    log = np.log(x[1:-1]) - np.log(np.subtract(1,x[1:-1])) + 0.5
    cos = -0.5*np.cos(np.pi*x) + 0.5

    fig = plt.figure(figsize=(9,3))
    square_ax = fig.add_axes([0.1,0.15,0.22,0.72]) # left
    log_ax  = fig.add_axes([0.41,0.145,0.225,0.74]) # middle
    cos_ax  = fig.add_axes([0.74,0.15,0.22,0.72]) # right
    
    for ax in [square_ax, log_ax, cos_ax]:
        ax.set_xlim([0, 1])
        ax.set_xlabel("Original score")
        ax.set_ylabel("Score after mapping")
        ax.plot(x, y, 'k-')
        ax.plot([0,1], [0.5,0.5], 'k--')

    square_ax.plot(x, square, 'r-')
    square_ax.set_ylim([0, 1])
    square_ax.set_aspect('equal')
    square_ax.plot([0.5,0.5], [0,1], 'k--')
    square_ax.annotate("(1)", xy=(0.05, 0.85))

    log_ax.plot(x[1:-1],log, 'r-')
    log_ax.set_ylim([-4.095, 5.095])
    log_ax.set_aspect(1.0/(2*4.595))
    log_ax.plot([0.5,0.5], [-4.095, 5.095], 'k--')
    log_ax.annotate("(2)", xy=(0.05, 3.8))

    cos_ax.plot(x,cos, 'r-')
    cos_ax.set_ylim([0, 1])
    cos_ax.set_aspect('equal')
    cos_ax.plot([0.5,0.5], [0,1], 'k--')
    cos_ax.annotate("(3)", xy=(0.05, 0.85))


    fu.show_or_save(fig, fig_name)


if __name__ == "__main__":
    prog_name = sys.argv[0]
    args = sys.argv[1:]
    
    if len(args) > 0 and args[0] == "--save":
        fig_name = prog_name.split('.')[0]
    else:
        fig_name = None

    make_fig(fig_name)
