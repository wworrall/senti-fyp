import sys

import matplotlib.pyplot as plt

import figutils as fu

def make_fig(fig_name=None):
    obj_scores = [0.1, 0.3, 0.5, 0.7, 0.9] # sample objective scores

    lines = {}
    for os in obj_scores:
        swn_min_pos = 0
        swn_max_pos = 1 - os # the case where neg score is 0

        min_sent = os/2 # min normalised senti score
        max_sent = swn_max_pos + os/2 # max normalised senti score

        lines[os] = ([swn_min_pos, swn_max_pos], [min_sent, max_sent])

    fig = plt.figure(figsize=(6,6))
    ax = fig.add_axes([0.12,0.10,0.8,0.8])
    ax.set_xlabel("Value of positive sentiment score in sentiment triplet",
            size=14)
    ax.set_ylabel("Map to Positive sentiment score", size=14)
    plt.tick_params(axis='both', which='major', labelsize=12)
    ax.set_xlim([0,1])
    ax.set_ylim([0,1])

    for os in obj_scores:
        label = "Objectivity = {}".format(os)
        ax.plot(lines[os][0], lines[os][1], label=label)
    ax.plot([0, 1], [0.5, 0.5], 'k--')

    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, labels, loc=4, fontsize=12)

    fu.show_or_save(fig, fig_name)


if __name__ == "__main__":
    prog_name = sys.argv[0]
    args = sys.argv[1:]
    
    if args[0] == "--save":
        fig_name = prog_name.split('.')[0]
    else:
        fig_name = None

    make_fig(fig_name)
