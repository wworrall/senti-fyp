import sys

import numpy as np
import matplotlib.pyplot as plt

import figutils as fu
from sentimodels import bowlinreg as blr

def make_plot(senti_method, map_to_pos, reg_terms, fig_prefix):

    model_name = "Simple BOW LR model"
    if map_to_pos:
        model_name = model_name + " (map to pos)"
    else:
        model_name = model_name + " (no map to pos)"

    print "{} with setiment method: {}".format(model_name, senti_method)
    print "with lin. regn terms: {}".format(reg_terms)

    # run the model
    Y_scores, Y_labels, Y_bins, X_scores, X_labels, X_bins = \
            blr.lin_reg(senti_method, map_to_pos, reg_terms)

    fu.print_model_accuracy(Y_scores, Y_labels, Y_bins, X_scores, X_labels,
            X_bins, senti_method)

    fu.make_scatter_plot(Y_scores, X_scores, model_name, senti_method,
            fig_prefix)

    fu.make_confmat_plot(Y_scores, Y_labels, X_scores, X_labels, senti_method,
            fig_prefix)


if __name__=="__main__":

    prog_name = sys.argv[0]
    args = sys.argv[1:]

    usage_text = """usage: {} senti_method map_to_pos reg_terms [--save]
    senti_method: the sentiment method to use - "AVERAGE" or "POLAR"
    map_to_pos  : map sentiment score triplet to 1 positivity value -
                  map_to_pos=0 or map_to_pos=1.
    reg_terms   : comma separated terms to in the linear regression model - see report
                  for full definitions. Any combination of original,square,log,cos
    [--save]    : save output to figure file."""

    if len(args) < 3:
        print usage_text.format(prog_name)
        sys.exit(1)

    senti_method = args[0]

    map_to_pos = bool(int(args[1].split('=')[1]))

    reg_terms = args[2].split(",")

    if len(args) > 3 and args[3] == "--save":
        fig_prefix = prog_name.split('.')[0]
        fig_prefix += "-" + senti_method.lower()
        if map_to_pos:
            fig_prefix += "-m2pos"
        else:
            fig_prefix += "-nom2pos"
        fig_prefix += "-" + ",".join(reg_terms)
    else:
        fig_prefix = None

    make_plot(senti_method, map_to_pos, reg_terms, fig_prefix)
    print ""
