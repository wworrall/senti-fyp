import time
import numpy as np

import datautils as du
import sentiutils as su

from sklearn import linear_model as lm

def concat_logs_rhs(x, num_source_cols=None):
    """concat log(x) - log(1-x) + 0.5 (element-wise) to rhs of array"""
    s = x[:, :num_source_cols] # source
    s[s > 0.99] = 0.99 # avoid returning inf
    s[s < 0.01] = 0.01 # avoid returning inf
    ls = np.log(s) - np.log(np.subtract(1,s)) + 0.5 # logs of source

    return np.concatenate((x, ls), axis=1)


def concat_squares_rhs(x, num_source_cols=None):
    """concat x^2 (element-wise) to rhs of array"""
    s = x[:, :num_source_cols] # source
    s2 = np.square(s) # squares of source

    return np.concatenate((x, s2), axis=1)


def concat_cos_rhs(x, num_source_cols=None):
    """concat -0.5*cos(pi*x) + 0.5 (element-wise) to rhs of array"""
    s = x[:, :num_source_cols] # source
    scos = -0.5*np.cos(np.pi*s) + 0.5 # cos func on source

    return np.concatenate((x, scos), axis=1)


def lin_reg(senti_method, map_to_pos, reg_terms, ww_senti_method="AVERAGE"):
    """Perform bag of words linear regression as folows:
- Use terms given in reg_terms
- no POS tag word sense disambiguation.
- WITHOUT the pos,neg,ob -> pos mapping function.
- Use the sentence sentiment method given eg. AVERAGE or POLAR."""

    # set data width to 1 (map to pos) or 3 (no map to pos)
    if map_to_pos:
        dw = 1
    else:
        dw = 3

    # load in the data
    train_data, test_data, dev_data =  du.load_data()


    # get bag of word sentiments for training data and test data
    #===========================================================
    train_sents = [ d['sen_str'] for d in train_data ]
    train_target_scores = np.asarray([ d['senti_score']
            for d in train_data ])

    # get sentiments for training sentences with a simple bag of words
    train_bow_senti_scores = np.empty((len(train_sents), dw))
    for idx, train_sent in enumerate(train_sents):
        bow_senti_score =  su.bow_senti_score(train_sent, senti_method,
                ww_senti_method, map_to_pos=map_to_pos)
        train_bow_senti_scores[idx, :] = bow_senti_score

    test_sents = [ d['sen_str'] for d in test_data ]
    test_target_scores = np.asarray([ d['senti_score']
            for d in test_data ])

    # get sentiments for test sentences with a simple bag of words
    test_bow_senti_scores = np.empty((len(test_sents), dw))
    for idx, test_sent in enumerate(test_sents):
        bow_senti_score =  su.bow_senti_score(test_sent, senti_method,
                ww_senti_method, map_to_pos=map_to_pos)
        test_bow_senti_scores[idx] = bow_senti_score


    # append the non-linear regression terms listed in reg_terms to the
    # sentiment scores matrices. Remove originals if not in reg_terms.
    #==================================================================
    # concatenate x^2
    if "square" in reg_terms:
        train_bow_senti_scores = concat_squares_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_squares_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # concatenate log(x) - log(1-x)
    if "log" in reg_terms:
        train_bow_senti_scores = concat_logs_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_logs_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # concatenate -0.5*cos(pi*x) + 0.5
    if "cos" in reg_terms:
        train_bow_senti_scores = concat_cos_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_cos_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # remove original terms
    if "original" not in reg_terms:
        train_bow_senti_scores = train_bow_senti_scores[:, dw:]
        test_bow_senti_scores = test_bow_senti_scores[:, dw:]


    # Learn a linear regression model
    #================================
    reg = lm.LinearRegression()
    start = time.time()
    reg.fit(train_bow_senti_scores, train_target_scores)
    end = time.time()
    print "Regression model learnt in {} seconds".format(end - start)
    print "Regresion model coef: {}, intercept: {}".format(reg.coef_,
            reg.intercept_)


    # Predict with linear regression model
    #=====================================
    pred_senti_scores = reg.predict(test_bow_senti_scores)

    # make seniment in range [0, 1]
    pred_senti_scores[pred_senti_scores > 1] = 1
    pred_senti_scores[pred_senti_scores < 0] = 0
    # classify
    pred_senti_classes = [ su.fine_grain_classify(s) for s in pred_senti_scores ]
    pred_senti_bins = [ su.bin_classify(s) for s in pred_senti_scores ]

    # get sst actual results
    sst_senti_scores = test_target_scores
    sst_senti_classes = [ su.fine_grain_classify(s) for s in sst_senti_scores ]
    sst_senti_bins = [ su.bin_classify(s) for s in sst_senti_scores ]

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins



def lin_reg_wsd(senti_method, map_to_pos, reg_terms, ww_senti_method="AVERAGE"):
    """Perform bag of words linear regression as folows:
- Use terms given in reg_terms
- POS tag word sense disambiguation.
- WITHOUT the pos,neg,ob -> pos mapping function.
- Use the sentence sentiment method given eg. AVERAGE or POLAR."""

    # set data width to 1 (map to pos) or 3 (no map to pos)
    if map_to_pos:
        dw = 1
    else:
        dw = 3

    # load in the data
    train_data, test_data, dev_data =  du.load_data()


    # get bag of word sentiments for training data and test data
    #===========================================================
    train_sents = [ d['sen_str'] for d in train_data ]
    train_target_scores = np.asarray([ d['senti_score']
            for d in train_data ])

    test_sents = [ d['sen_str'] for d in test_data ]
    test_target_scores = np.asarray([ d['senti_score']
        for d in test_data ])

    # parse both train and test in to UD format
    train_conllu_texts = du.run_parser_conllu(train_sents)
    test_conllu_texts = du.run_parser_conllu(test_sents)

    # parse conllus in to dict form
    train_conllus = [ du.parse_conllu_text(ct) for ct in train_conllu_texts ]
    test_conllus = [ du.parse_conllu_text(ct) for ct in test_conllu_texts ]

    # get sentiments for training sentences with a simple bag of words
    train_bow_senti_scores = np.empty((len(train_conllus), dw))
    for idx, train_conllu in enumerate(train_conllus):
        bow_senti_score =  su.bow_senti_score_wsd(train_conllu, senti_method,
                ww_senti_method, map_to_pos=map_to_pos)
        train_bow_senti_scores[idx, :] = bow_senti_score

    # get sentiments for test sentences with a simple bag of words
    test_bow_senti_scores = np.empty((len(test_conllus), dw))
    for idx, test_conllu in enumerate(test_conllus):
        bow_senti_score =  su.bow_senti_score_wsd(test_conllu, senti_method,
                ww_senti_method, map_to_pos=map_to_pos)
        test_bow_senti_scores[idx, :] = bow_senti_score


    # append the non-linear regression terms listed in reg_terms to the
    # sentiment scores matrices. Remove originals if not in reg_terms.
    #==================================================================
    # concatenate x^2
    if "square" in reg_terms:
        train_bow_senti_scores = concat_squares_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_squares_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # concatenate log(x) - log(1-x)
    if "log" in reg_terms:
        train_bow_senti_scores = concat_logs_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_logs_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # concatenate -0.5*cos(pi*x) + 0.5
    if "cos" in reg_terms:
        train_bow_senti_scores = concat_cos_rhs(train_bow_senti_scores,
                num_source_cols=dw)
        test_bow_senti_scores = concat_cos_rhs(test_bow_senti_scores,
                num_source_cols=dw)

    # remove original terms
    if "original" not in reg_terms:
        train_bow_senti_scores = train_bow_senti_scores[:, dw:]
        test_bow_senti_scores = test_bow_senti_scores[:, dw:]


    # Learn a linear regression model
    #================================
    reg = lm.LinearRegression()
    start = time.time()
    reg.fit(train_bow_senti_scores, train_target_scores)
    end = time.time()
    print "Regression model learnt in {} seconds".format(end - start)
    print "Regresion model coef: {}, intercept: {}".format(reg.coef_,
            reg.intercept_)


    # Predict with linear regression model
    #=====================================
    pred_senti_scores = reg.predict(test_bow_senti_scores)

    # make seniment in range [0, 1]
    pred_senti_scores[pred_senti_scores > 1] = 1
    pred_senti_scores[pred_senti_scores < 0] = 0
    # classify
    pred_senti_classes = [ su.fine_grain_classify(s) for s in pred_senti_scores ]
    pred_senti_bins = [ su.bin_classify(s) for s in pred_senti_scores ]

    # get sst actual results
    sst_senti_scores = test_target_scores
    sst_senti_classes = [ su.fine_grain_classify(s) for s in sst_senti_scores ]
    sst_senti_bins = [ su.bin_classify(s) for s in sst_senti_scores ]

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins


def make_composite_bags(conllu):
    bows = {
            'neg': [],
            'other': [],
            }

    # search for negation in conllu:
    neg_wl = next((wl for wl in conllu if wl['deprel'] == 'neg'), None)
    if neg_wl:
        neg_parent = next((wl for wl in conllu
                if wl['id'] == neg_wl['head']))

        while True:
            # check for termination: neg_parent no longer in conllu
            if neg_parent not in conllu:
                break

            node = neg_parent
            while True:
                ch_node = next((wl for wl in conllu
                        if wl['head'] == node['id']), None)
                if ch_node is not None:
                    node = ch_node; continue
                else:
                    bows['neg'].append(node)
                    conllu = filter(lambda wl: wl is not node, conllu)
                    break

    # append words that are left to 'other' bag
    bows['other'].extend(conllu)

    return bows


def lin_reg_comp(senti_method, map_to_pos, reg_terms, ww_senti_method="AVERAGE"):
    """Note, this is an extension of the BOW LR WSD model, where we now make our
bag of words score for a sentence from a composition of bags of words. In
particular we will be looking at words that are negated.
Perform bag of words linear regression as folows:
- Construct composite bags of words, eg. negated
- Use terms given in reg_terms
- POS tag word sense disambiguation.
- WITHOUT the pos,neg,ob -> pos mapping function.
- Use the sentence sentiment method given eg. AVERAGE or POLAR."""

    # set data width to 1 (map to pos) or 3 (no map to pos)
    if map_to_pos:
        dw = 1
    else:
        dw = 3

    # load in the data
    train_data, test_data, dev_data =  du.load_data()

    # get bag of word sentiments for training data and test data
    #===========================================================
    train_sents = [ d['sen_str'] for d in train_data ]
    train_target_scores = np.asarray([ d['senti_score']
        for d in train_data ])

    test_sents = [ d['sen_str'] for d in test_data ]
    test_target_scores = np.asarray([ d['senti_score']
        for d in test_data ])

    # parse both train and test in to UD format
    train_conllu_texts = du.run_parser_conllu(train_sents)
    test_conllu_texts = du.run_parser_conllu(test_sents)

    # parse conllus in to dict form
    train_conllus = [ du.parse_conllu_text(ct) for ct in train_conllu_texts ]
    test_conllus = [ du.parse_conllu_text(ct) for ct in test_conllu_texts ]

    bag_names = ['neg', 'other']

    train_bow_senti_scores = np.empty((len(train_conllus), dw*len(bag_names)))
    # get sentiments for training sentences with composite bag of words
    for idx, train_conllu in enumerate(train_conllus):
        bows = make_composite_bags(train_conllu)
        for jdx, bn in enumerate(bag_names):
            bow_senti_score =  su.bow_senti_score_wsd(bows[bn], senti_method,
                    ww_senti_method, map_to_pos=map_to_pos)
            train_bow_senti_scores[idx, jdx*dw:(jdx+1)*dw] = bow_senti_score

    test_bow_senti_scores = np.empty((len(test_conllus), dw*len(bag_names)))
    # get sentiments for test sentences with composite bag of words
    for idx, test_conllu in enumerate(test_conllus):
        bows = make_composite_bags(test_conllu)
        for jdx, bn in enumerate(bag_names):
            bow_senti_score =  su.bow_senti_score_wsd(bows[bn], senti_method,
                    ww_senti_method, map_to_pos=map_to_pos)
            test_bow_senti_scores[idx, jdx*dw:(jdx+1)*dw] = bow_senti_score


    # append the non-linear regression terms listed in reg_terms to the
    # sentiment scores matrices. Remove originals if not in reg_terms.
    #==================================================================
    odw = dw * len(bag_names) # data width for 'original' sentiment scores

    # concatenate x^2
    if "square" in reg_terms:
        train_bow_senti_scores = concat_squares_rhs(train_bow_senti_scores,
                num_source_cols=odw)
        test_bow_senti_scores = concat_squares_rhs(test_bow_senti_scores,
                num_source_cols=odw)

    # concatenate log(x) - log(1-x)
    if "log" in reg_terms:
        train_bow_senti_scores = concat_logs_rhs(train_bow_senti_scores,
                num_source_cols=odw)
        test_bow_senti_scores = concat_logs_rhs(test_bow_senti_scores,
                num_source_cols=odw)

    # concatenate -0.5*cos(pi*x) + 0.5
    if "cos" in reg_terms:
        train_bow_senti_scores = concat_cos_rhs(train_bow_senti_scores,
                num_source_cols=odw)
        test_bow_senti_scores = concat_cos_rhs(test_bow_senti_scores,
                num_source_cols=odw)

    # remove original terms
    if "original" not in reg_terms:
        train_bow_senti_scores = train_bow_senti_scores[:, odw:]
        test_bow_senti_scores = test_bow_senti_scores[:, odw:]


    # Learn a linear regression model
    #================================
    reg = lm.LinearRegression()
    start = time.time()
    reg.fit(train_bow_senti_scores, train_target_scores)
    end = time.time()
    print "Regression model learnt in {} seconds".format(end - start)
    print "Regresion model coef: {}, intercept: {}".format(reg.coef_,
            reg.intercept_)


    # Predict with linear regression model
    #=====================================
    pred_senti_scores = reg.predict(test_bow_senti_scores)

    # make seniment in range [0, 1]
    pred_senti_scores[pred_senti_scores > 1] = 1
    pred_senti_scores[pred_senti_scores < 0] = 0
    # classify
    pred_senti_classes = [ su.fine_grain_classify(s) for s in pred_senti_scores ]
    pred_senti_bins = [ su.bin_classify(s) for s in pred_senti_scores ]

    # get sst actual results
    sst_senti_scores = test_target_scores
    sst_senti_classes = [ su.fine_grain_classify(s) for s in sst_senti_scores ]
    sst_senti_bins = [ su.bin_classify(s) for s in sst_senti_scores ]

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins
