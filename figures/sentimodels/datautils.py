import os
import codecs
import subprocess


treebank_dir = os.path.join('.', 'stanfordSentimentTreebank')


def sentence_standardiser(s):
    """Fix issues with sentence string such as -LRB- instead of (."""
    s = s.replace("-LRB-", "(")
    s = s.replace("-RRB-", ")")
    s = s.replace("\\/", "/")
    s = s.replace("``", "\"")
    s = s.replace("''", "\"")

    return s


def datasetSentences():
    """Read from datasetSentences.txt file and return a dict mapping ids to
sentences. NB. this file uses latin-1 (iso-8859-1) char encoding so we convert."""

    datasetSentences = {}
    with codecs.open(os.path.join(treebank_dir, 'datasetSentences.txt'),
            encoding='utf-8') as f:
        for line in f.readlines()[1:]: # skip 1st line
            # convert latin-1 to utf-8
            line = line.encode('iso-8859-1').decode('utf8')
            s_id, sentence = line.split('\t')
            sentence = sentence_standardiser(sentence)
            datasetSentences[int(s_id)] = sentence.strip()

    return datasetSentences


def dictionary():
    """Read all phrases from dictionary.txt returning dict mapping ids to
phrases."""

    phrases = {}
    with codecs.open(os.path.join(treebank_dir, 'dictionary.txt'),
            encoding='utf-8') as f:
        for line in f.readlines():
            phrase, p_id = line.split('|')
            phrase = sentence_standardiser(phrase)
            phrases[int(p_id)] = phrase.strip()

    return phrases


def datasetSplit():
    """Read the dataset splits from datasetSplit.txt returning a dict of lists
for training, dev and test phrases."""

    data_splits = {
            "train": [],  # 1 = train
            "test": [],   # 2 = test
            "dev": [],    # 3 = dev
            }

    with codecs.open(os.path.join(treebank_dir, 'datasetSplit.txt'),
            encoding='utf-8') as f:
        for line in f.readlines()[1:]: # skip 1st line
            sen_id, cat = line.split(',')
            if int(cat) is 1:
                data_splits["train"].append(int(sen_id))
            elif int(cat) is 2:
                data_splits["test"].append(int(sen_id))
            elif int(cat) is 3:
                data_splits["dev"].append(int(sen_id))
            else:
                raise ValueError("Unknown category id")
    
    return data_splits


def sentiment_labels():
    """Read the sentiment scores for all phrases from sentiment_labels.txt
returning dict mapping phrase id to sentiment score."""

    sent_scores = {}

    with codecs.open(os.path.join(treebank_dir, 'sentiment_labels.txt'),
            encoding='utf-8') as f:
        for line in f.readlines()[1:]: # skip 1st line
            p_id, sent_score = line.split('|')
            sent_scores[int(p_id)] = float(sent_score)

    return sent_scores


def load_data():
    """Return three lists of data (train/test/dev) where each sentence in each
list is a dict following fields:
sen_id, sen_str, senti_score, phrase_id.
Load data from sst-sentence-sentiment.txt if it exists, if not, make it."""

    train = []
    test = []
    dev= []

    # load data generated from stanford sentiment tree bank
    try:
        with codecs.open('sst-sentence-sentiment.txt', encoding='utf-8') as f:
            dataset = None # the dataset to append to
            for l in f.readlines()[1:]: # skip 1st line
                l = l.strip() # remove newline
                if l == u"[train]":
                    dataset = train
                elif l == u"[test]":
                    dataset = test
                elif l == u"[dev]":
                    dataset = dev
                else:
                    sen_id, sen_str, senti_score, phrase_id = l.split("|")
                    d = {
                        "sen_id": int(sen_id),
                        "sen_str": sen_str,
                        "senti_score": float(senti_score),
                        "phrase_id": int(phrase_id),
                    }
                    dataset.append(d)

    # data file doesn't exist, make it
    except IOError:

        # load resources from stanford sentiment treebank
        sentences_dict = datasetSentences()
        phrases_dict = dictionary()
        data_splits = datasetSplit()
        senti_scores = sentiment_labels()

        # loop through sentences
        for sen_id, sen_str in sentences_dict.items():
            # first we must match the sentence with the phrase id
            phrase_id = phrases_dict.keys()[phrases_dict.values().index(sen_str)]
            senti_score = senti_scores[phrase_id]
            
            d = { "sen_id": sen_id, "sen_str": sen_str, "senti_score": senti_score,
                    "phrase_id": phrase_id }

            if sen_id in data_splits["train"]:
                train.append(d)
            elif sen_id in data_splits["test"]:
                test.append(d)
            elif sen_id in data_splits["dev"]:
                dev.append(d)
            else:
                raise ValueError("sen_id not found in train/test/dev")

        with codecs.open('sst-sentence-sentiment.txt', 'w', encoding='utf-8') as f:
            f.write(u"sen_id|sen_str|senti_score|phrase_id\n")
            
            # write training data
            f.write(u"[train]\n")
            for d in train:
                f.write(u"{}|{}|{}|{}\n".format(d["sen_id"], d["sen_str"],
                        d["senti_score"], d["phrase_id"]))

            # write test data
            f.write(u"[test]\n")
            for d in test:
                f.write(u"{}|{}|{}|{}\n".format(d["sen_id"], d["sen_str"],
                        d["senti_score"], d["phrase_id"]))

            # write dev data
            f.write(u"[dev]\n")
            for d in dev:
                f.write(u"{}|{}|{}|{}\n".format(d["sen_id"], d["sen_str"],
                        d["senti_score"], d["phrase_id"]))

    return train, test, dev



def run_parser_conllu(sentences):
    """Run pmpf on a list of sentences, obtaining output in conllu format.
Return list of strings each containing a conllu text expression."""

    # preprocessing:
    for idx, s in enumerate(sentences):
        sentences[idx] = sentence_standardiser(s)

    # write each sentence to an input file
    for i, s in enumerate(sentences):
        filename = os.path.join('tmp_data', str(i) + '.sentence')
        with codecs.open(filename, 'w', encoding='utf-8') as f:
            f.write(s)

    # create input file list
    filename = os.path.join('tmp_data', 'input_file_list.txt')
    with codecs.open(filename, 'w',
            encoding='utf-8') as f:
        for i in range(len(sentences)):
            sentence_fname = os.path.join('tmp_data', str(i) + '.sentence' + '\n')
            f.write(sentence_fname)

    # run the parser
    parser_cmd = [
            "java", "-Xmx2g", "-cp", "stanford-corenlp-full-2016-10-31/*",
            "edu.stanford.nlp.pipeline.StanfordCoreNLP",
            "-annotators", "tokenize,ssplit,pos,depparse",
            "-filelist", os.path.join('tmp_data', 'input_file_list.txt'),
            "-outputDirectory", "tmp_data",
            "-outputFormat", "conllu"
            ]
    parser_proc = subprocess.Popen(parser_cmd, stderr=subprocess.PIPE)
    parser_out, parser_err = parser_proc.communicate()
    if parser_proc.returncode is not 0:
        err_str = """Parser process failed with non-zero return code and the
following stdout and std err:
--------------- BEGIN PARSER STDOUT ---------------
{}
--------------- BEGIN PARSER STDERR ---------------
{}
"""
        raise ValueError(err_str.format(parser_out, parser_err))

    # read output and delete files
    conllus = []
    os.remove(os.path.join('tmp_data', 'input_file_list.txt'))
    for i in range(len(sentences)):
        with codecs.open(os.path.join('tmp_data', str(i) + '.sentence.conllu'),
            encoding='utf-8') as f:
            conllus.append(f.read().rstrip())
        os.remove(os.path.join('tmp_data', str(i) + '.sentence'))
        os.remove(os.path.join('tmp_data', str(i) + '.sentence.conllu'))

    return conllus



def parse_conllu_text(conllu_text):
    """Read in conllu as a string (or list) and return list of dicts mapping to
each field"""

    conllu_lines = conllu_text.splitlines()

    # remove empty lines at start and end
    try:
        while (conllu_lines[0].isspace() or conllu_lines[0] == ""):
            conllu_lines.pop(0)
        while (conllu_lines[-1].isspace() or conllu_lines[-1] == ""):
            conllu_lines.pop(-1)
    except IndexError:
        raise ValueError("No CoNLL-U expression found.") # validity check

    line_dicts = [] # each line of conllu stored as a dict
    ld_keys = ["id", "form", "lemma", "upostag", "xpostag", "feats",
            "head", "deprel", "deps", "misc"]

    for line in conllu_lines:
        # if we've reached an empty line we got to the end of the conllu parse
        if line is "":
            break

        terms = line.split()
        if len(terms) is not 10: # validity check
            return {}
            raise ValueError("Invalid CoNLL-U line: '{}' found - must have 10 "\
                    "terms.".format(line))

        line_dict = {}
        for idx, key in enumerate(ld_keys):
            if key is "id" or key is "head":
                line_dict[key] = int(terms[idx])
            if terms[idx] is "_":
                line_dict[key] = None
            else:
                line_dict[key] = terms[idx]

        line_dicts.append(line_dict)

    return line_dicts


if __name__=="__main__":
    """This is just all testing"""
    s = datasetSentences()
    s_keys = sorted(s.keys())
    for k in s_keys:
        print k, s[k]
        if k > 8:
            break

    p = dictionary()
    p_keys = sorted(p.keys())
    for k in p_keys:
        print k, p[k]
        if k > 20:
            break

    dsp = datasetSplit()
    print "size of splits: train={}, test={}, dev={}".format(len(dsp["train"]),
            len(dsp["test"]), len(dsp["dev"]))

    sent_scores = sentiment_labels()
    ctr = 0
    p_ids = sorted(sent_scores.keys())
    for i in p_ids:
        print i, sent_scores[i]
        ctr+=1
        if ctr > 10:
            break

    train, test, dev = load_data()

    for i in range(9):
        print train[i]
    print "size of train: {}".format(len(train))
    print ""

    for i in range(9):
        print test[i]
    print "size of test: {}".format(len(test))
    print ""

    for i in range(9):
        print dev[i]
    print "size of dev: {}".format(len(dev))
