import numpy as np
from nltk.corpus import sentiwordnet as swn

import datautils as du


def ssm_average_no_map_to_pos(word_sentis):
    """Average all the pos, neg, obj scores in the bag"""
    word_sentis = np.array(word_sentis)
    # check for empty array first
    if word_sentis.size == 0:
        return np.array([[0, 0, 1]]) # all objective

    # axis = 0 => column-wise means
    return np.mean(word_sentis, axis=0)


def ssm_average(word_sentis):
    """sentence sentiment method: take an average across the words that
have sentiment in the sentence"""

    try:
        senti_score = sum(word_sentis)/len(word_sentis)
    except ZeroDivisionError:
        # we couldn't get any sentiment from the sentence so assign neutral
        senti_score = 0.5

    return senti_score


def ssm_polar_no_map_to_pos(word_sentis):
    """Return the most polar pos, neg, obj score from the bag"""
    word_sentis = np.array(word_sentis)


    # check for empty array first
    if word_sentis.size == 0:
        return np.array([[0, 0, 1]]) # all objective

    # find row indices of the most polar pos and neg scores
    max_pos_ind, max_neg_ind = np.argmax(word_sentis[:, :2], axis=0)

    # compare the rows to find out which is more polar and return that row
    if word_sentis[max_pos_ind,:][0] > word_sentis[max_neg_ind,:][1]:
        return word_sentis[max_pos_ind,:]
    else:
        return word_sentis[max_neg_ind,:]


def ssm_polar(word_sentis):
    """sentence sentiment method: take the word with the strogest sentiment to
    represent the sentiment of the sentence. Do this by finding the word with
    the biggest distance from 0.5"""

    # we couldn't get any sentiment from the sentence so assign neutral
    if not word_sentis:
        return 0.5

    distances = [ abs(0.5-s) for s in word_sentis ]
    ind = distances.index(max(distances))

    return word_sentis[ind]


def bow_senti_score(sentence, senti_method, ww_senti_method, map_to_pos=True):
    """get sentiment for a bag of words (tokens) from sentiwordnet with no pos
    tag word sense disambiguation."""

    tokens = sentence.split()

    # get wordwise sentiments
    word_sentis = []

    for tok in tokens:
        word_sentis.append(swn_sentiment(tok, ww_senti_method,
                map_to_pos=map_to_pos))

    # remove words with no sentiment from swn
    word_sentis = [ ws for ws in word_sentis if ws is not None]

    # use method given
    if senti_method == "AVERAGE":
        if map_to_pos:
            senti_score = ssm_average(word_sentis)
        else:
            senti_score = ssm_average_no_map_to_pos(word_sentis)
    elif senti_method == "POLAR":
        if map_to_pos:
            senti_score = ssm_polar(word_sentis)
        else:
            senti_score = ssm_polar_no_map_to_pos(word_sentis)
    else:
        raise ValueError('Unknown sentence sentiment method')

    return senti_score


def bow_senti_score_wsd(conllu, senti_method, ww_senti_method, map_to_pos=True):
    """get sentiment for a bag of words (tokens) from sentiwordnet with POS
    tag word sense disambiguation."""

    # get wordwise sentiments
    word_sentis = []
    for wl in conllu:
        tag = swn_tag(wl)
        word_sentis.append(swn_sentiment(wl['form'], ww_senti_method,
                tag=tag, map_to_pos=map_to_pos))

    # remove words with no sentiment from swn
    word_sentis = [ ws for ws in word_sentis if ws is not None]

    # use method given
    if senti_method == "AVERAGE":
        if map_to_pos:
            senti_score = ssm_average(word_sentis)
        else:
            senti_score = ssm_average_no_map_to_pos(word_sentis)
    elif senti_method == "POLAR":
        if map_to_pos:
            senti_score = ssm_polar(word_sentis)
        else:
            senti_score = ssm_polar_no_map_to_pos(word_sentis)
    else:
        raise ValueError('Unknown sentence sentiment method')

    return senti_score


def swn_tag(wl):
    """extract swn compatible POS tag from a word line (wl) of conllu format.
Tags are: a, r, n, v, NoneType (no tag compatible tag)"""

    if wl['xpostag'].startswith('JJ'):
        return 'a'
    elif wl['xpostag'].startswith('RB'):
        return 'r'
    elif wl['xpostag'].startswith('NN'):
        return 'n'
    elif wl['xpostag'].startswith('VB'):
        return 'v'
    else:
        return None


pos_tag_map = {
        'adj': 'a',
        'a': 'JJ',
        'adverb': 'r',
        'r': 'RB',
        'noun': 'n',
        'n': 'NN',
        'verb': 'v',
        'v': 'VB',
        'other': None,
        }


def bin_classify(senti_score):
    """Classify in to binary sentiment classes '-' and '+'."""
    if senti_score < 0.5:
        return '-'
    elif senti_score <= 1:
        return '+'
    else:
        raise ValueError("Invalid sentiment score - must be in range [0, 1]")


def fine_grain_classify(senti_score):
    """From SST README.txt: recover the 5 classes by mapping the positivity
probability using the following cut-offs
[0, 0.2], (0.2, 0.4], (0.4, 0.6], (0.6, 0.8], (0.8, 1.0]."""

    if senti_score <= 0.2:
        return "--"
    elif senti_score <= 0.4:
        return "-"
    elif senti_score <= 0.6:
        return "0"
    elif senti_score <= 0.8:
        return "+"
    elif senti_score <= 1:
        return "++"
    else:
        raise ValueError("Invalid sentiment score - must be in range [0, 1]")


def norm_swn_sentiment(pos, neg, obj):
    """normalise the 3-valued sentiment (pos/neg/obj) score given by
sentiwordnet. With swn pos+neg+obj = 1. We want sentiment on a 0 -> 1 scale
representing positivity. Therefore, return the pos score + obj/2. This has the
effect of redistributing the objectivity score evenly between pos and neg."""

    return pos + obj/2


def swn_sentiment(w, method, tag=None, map_to_pos=True):
    """Get sentiment for a single word from sentiwordnet:
AVERAGE - take average across synset
POPULAR - sentiment of sentiwordnets most popular def.
Map the score to single positivity value if map_to_pos is True"""

    # get the synsets using a tag if we have one
    if tag:
        ss = list(swn.senti_synsets(w, tag))
        # if ss is empty we'll try for the word with out the tag
        if not ss:
            ss = list(swn.senti_synsets(w))
    else:
        ss = list(swn.senti_synsets(w))

    if method is "AVERAGE":
        if not ss:
            return None
        else:
            all_pos=[]
            all_neg=[]
            all_obj=[]
            for x in ss:
                all_pos.append(x.pos_score())
                all_neg.append(x.neg_score())
                all_obj.append(x.obj_score())

            pos, neg, obj = (sum(all_pos)/len(all_pos), sum(all_neg)/len(all_neg),
                    sum(all_obj)/len(all_obj))

    elif method is "POPULAR":
        if not ss:
            return None
        else:
            pos = ss[0].pos_score()
            neg = ss[0].neg_score()
            obj = ss[0].obj_score()

    else:
        raise ValueError("Unknown sentiment method")

    if not map_to_pos:
        return [pos, neg, obj]
    else:
        return norm_swn_sentiment(pos, neg, obj)
