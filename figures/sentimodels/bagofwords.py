import time
import numpy as np
from sklearn import linear_model as lm

import datautils as du
import sentiutils as su


def bag_of_words(senti_method, ww_senti_method="AVERAGE"):
    """Perform bag of words model on the test data using the sentence sentiment
method give eg. AVERAGE or POLAR"""

    _, test_data, _ =  du.load_data()

    # for predictions
    pred_senti_scores = np.empty((len(test_data), 1))
    pred_senti_classes = [] # 5 class sentiment
    pred_senti_bins = [] # binary sentiment

    # actual values
    sst_senti_scores = np.empty((len(test_data), 1))
    sst_senti_classes = []
    sst_senti_bins = []

    for idx, d in enumerate(test_data):

        sst_senti_score = d["senti_score"] # sst sentiment score
        sen_str = d["sen_str"]
        tokens = sen_str.split()

        # get wordwise sentiments
        word_sentis = []
        for tok in tokens:
            word_sentis.append(su.swn_sentiment(tok, ww_senti_method))

        # remove words with no sentiment from swn (other option is to set them to
        # neutral sentiment?)
        word_sentis = [ ws for ws in word_sentis if ws is not None]

        # use method given
        if senti_method == "AVERAGE":
            senti_score = su.ssm_average(word_sentis)
        elif senti_method == "POLAR":
            senti_score = su.ssm_polar(word_sentis)
        else:
            raise ValueError('Unknown sentence sentiment method')

        # save predicted and actual senti scores and corresponing classes
        pred_senti_scores[idx] = senti_score
        pred_senti_classes.append(su.fine_grain_classify(senti_score))
        pred_senti_bins.append(su.bin_classify(senti_score))
        sst_senti_scores[idx] = sst_senti_score
        sst_senti_classes.append(su.fine_grain_classify(sst_senti_score))
        sst_senti_bins.append(su.bin_classify(sst_senti_score))

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins


def bag_of_words_tag(senti_method, tag, ww_senti_method="AVERAGE"):
    """Perform bag of words model on the test data using the sentence sentiment
method give eg. AVERAGE or POLAR on adjectives only. The tag argument specifies
the word type to take sentmiment over:
'n' - NOUN
'v' - VERB
'a' - ADJECTIVE
'r' - ADVERB
If no adjectives are found, return the sentiment of the sentence using the other
words."""

    _, test_data, _ =  du.load_data()

    # for predictions
    pred_senti_scores = np.empty((len(test_data), 1))
    pred_senti_classes = [] # 5 class sentiment
    pred_senti_bins = [] # binary sentiment

    # actual values
    sst_senti_scores = np.empty((len(test_data), 1))
    sst_senti_classes = []
    sst_senti_bins = []

    sentences = []

    for idx, d in enumerate(test_data):
        sst_senti_scores[idx] = d["senti_score"]
        sst_senti_classes.append(su.fine_grain_classify(d["senti_score"]))
        sst_senti_bins.append(su.bin_classify(d["senti_score"]))
        sentences.append(d["sen_str"])

    # get dependency parses in conllu texts form
    conllu_texts = du.run_parser_conllu(sentences)
    # parse conllus in to dict form
    conllus = [ du.parse_conllu_text(ct) for ct in conllu_texts ]

    for idx, conllu in enumerate(conllus):
        word_set = [] # set of words to take sentiment from
        # loop wordlines
        for wl in conllu:
            if wl['xpostag'].startswith(su.pos_tag_map[tag]):
                word_set.append(wl['form'])

        # get sentiments of words in word set
        word_sentis = []
        for word in word_set:
            word_sentis.append(su.swn_sentiment(word, ww_senti_method,
                tag=tag))

        # remove words with no sentiment from swn (other option is to set them to
        # neutral sentiment?)
        word_sentis = [ ws for ws in word_sentis if ws is not None]

        # use method given
        if senti_method == "AVERAGE":
            senti_score = su.ssm_average(word_sentis)
        elif senti_method == "POLAR":
            senti_score = su.ssm_polar(word_sentis)
        else:
            raise ValueError('Unknown sentence sentiment method')

        # save predicted and actual senti scores and corresponing classes
        pred_senti_scores[idx] = senti_score
        pred_senti_classes.append(su.fine_grain_classify(senti_score))
        pred_senti_bins.append(su.bin_classify(senti_score))

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins


def bow_pos_senti_scores(sentences, senti_method, ww_senti_method, tags):
    """This is a helper function used by pos linear regression function, it
takes in a list of sentences and returns an array of sentiment scores where the
colums are the sentiment calclulated from using the pos tags passed in in the
tags list. At most the tags list can be:
['adj', 'adverb', 'noun', 'verb', 'other']"""
    
    tag_idx_map = { 'adj': 0, 'adverb': 1, 'noun': 2, 'verb': 3, 'other': 4 }

    # get dependency parses in conllu texts form
    conllu_texts = du.run_parser_conllu(sentences)
    # parse conllus in to dict form
    conllus = [ du.parse_conllu_text(ct) for ct in conllu_texts ]

    # train senti scores matrix will have columns adj, adverb, noun, verb,
    # other - size |N| x 5 where |N| is the length of training sentences
    bow_senti_scores = np.empty((len(sentences),5))

    for idx, conllu in enumerate(conllus):
        word_bags = {
            'adj': [],
            'adverb': [],
            'noun': [],
            'verb': [],
            'other': [],
            }

        # loop wordlines
        for wl in conllu:
            if wl['xpostag'].startswith('JJ'):
                word_bags['adj'].append(wl['form'])
            elif wl['xpostag'].startswith('RB'):
                word_bags['adverb'].append(wl['form'])
            elif wl['xpostag'].startswith('NN'):
                word_bags['noun'].append(wl['form'])
            elif wl['xpostag'].startswith('VB'):
                word_bags['verb'].append(wl['form'])
            else:
                word_bags['other'].append(wl['form'])

        bag_scores = {}
        for pos_name, words in word_bags.iteritems():
            tag = su.pos_tag_map[pos_name]
            word_sentis = []
            for w in words:
                word_sentis.append(su.swn_sentiment(w, ww_senti_method, tag))

            # remove words with no sentiment from swn (other option is to set them to
            # neutral sentiment?)
            word_sentis = [ ws for ws in word_sentis if ws is not None]

            # use senti method given
            if senti_method == "AVERAGE":
                senti_score = su.ssm_average(word_sentis)
            elif senti_method == "POLAR":
                senti_score = su.ssm_polar(word_sentis)
            else:
                raise ValueError('Unknown sentence sentiment method')

            bag_scores[pos_name] = senti_score

        # store sentiments
        row = [bag_scores['adj'], bag_scores['adverb'], bag_scores['noun'],
                bag_scores['verb'], bag_scores['other']]
        bow_senti_scores[idx] = row

    idx_list = [ tag_idx_map[t] for t in tags ]
    return bow_senti_scores[:,idx_list]


def bow_pos_lin_reg(senti_method, tags, ww_senti_method="AVERAGE"):
    """Learn a linear regression model on nouns, verbs, adverbs and adjectives,
treating each set as a bag of words in it's own right. Use the sentences in
in the training data to learn the model and test on the sentences in the
test data."""

    train_data, test_data, dev_data =  du.load_data()

    # LEARNING #
    train_sents = [ d['sen_str'] for d in train_data ]
    train_target_scores = np.asarray([ d['senti_score']
            for d in train_data ])
    # for each sentence get the bag of nouns, verbs, adverbs and adjectives
    train_bow_senti_scores = bow_pos_senti_scores(train_sents, senti_method,
            ww_senti_method, tags)

    start = time.time()
    # Learn a linear regression model
    reg = lm.LinearRegression()
    reg.fit(train_bow_senti_scores, train_target_scores)
    end = time.time()
    print "Regression model learnt in {} seconds".format(end-start)
    print "Regresion model coef: {}, intercept: {}".format(reg.coef_,
            reg.intercept_)

    # TESTING #
    test_sents = [ d['sen_str'] for d in test_data ]
    test_target_scores = np.asarray([ d['senti_score']
            for d in test_data ])
    # calculate our pos bow sentiments for test data
    test_bow_senti_scores = bow_pos_senti_scores(test_sents, senti_method,
            ww_senti_method, tags)

    # use regression model to predict
    pred_senti_scores = reg.predict(test_bow_senti_scores)
    pred_senti_classes = [ su.fine_grain_classify(s) for s in pred_senti_scores ]
    pred_senti_bins = [ su.bin_classify(s) for s in pred_senti_scores ]

    # get sst actual results
    sst_senti_scores = test_target_scores
    sst_senti_classes = [ su.fine_grain_classify(s) for s in sst_senti_scores ]
    sst_senti_bins = [ su.bin_classify(s) for s in sst_senti_scores ]

    return pred_senti_scores, pred_senti_classes, pred_senti_bins,\
            sst_senti_scores, sst_senti_classes, sst_senti_bins
