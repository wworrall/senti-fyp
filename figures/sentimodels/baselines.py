import datautils as du
import numpy as np


def ur_err_test_data():
    _, test_data, _ =  du.load_data()

    # Calculate the mean squared error that we would get if we simply uniformly
    # randomly guessed the sentiment scores:
    #
    # expected err = 1/3 + 1/N sum ( xi^2 - xi ) for i in range N

    # get all senti scores in test data as x
    x = np.array([ d["senti_score"] for d in test_data ])
    x2 = np.square(x)
    N = len(x)

    #     err =      1/3           +       1/N        * (  sum(x^2)  -   sum(x)  )
    exp_error = np.divide(1.0,3.0) + np.divide(1.0,N) * ( np.sum(x2) - np.sum(x) )

    return exp_error

def med_err_test_data():
    _, test_data, _ =  du.load_data()

    # Calculate the mean squared error that we would get if we simply guessed
    # the sentiment score to be 0.5 for all test sentences

    # get all senti scores in test data as x
    x = np.array([ d["senti_score"] for d in test_data ])
    y = np.ones(len(x))*0.5

    err = y - x
    exp_error = np.divide(sum(err**2), len(x))

    return exp_error
