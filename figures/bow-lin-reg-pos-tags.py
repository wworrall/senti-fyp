import sys

import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize

import figutils as fu
from sentimodels import bagofwords as bow
from sentimodels import baselines as bl

def make_plot(senti_method, tags, fig_prefix):

    model_name = "POS tags BOW LR model"

    print "{} with setiment method: {}".format(model_name, senti_method)
    print "with tags: {}".format(tags)

    # run the model
    Y_scores, Y_labels, Y_bins, X_scores, X_labels, X_bins = \
            bow.bow_pos_lin_reg(senti_method, tags)

    fu.print_model_accuracy(Y_scores, Y_labels, Y_bins, X_scores, X_labels,
            X_bins, senti_method)

    fu.make_scatter_plot(Y_scores, X_scores, model_name, senti_method,
            fig_prefix)

    fu.make_confmat_plot(Y_scores, Y_labels, X_scores, X_labels, senti_method,
            fig_prefix)


if __name__=="__main__":

    prog_name = sys.argv[0]
    args = sys.argv[1:]

    usage_text = """usage: {} senti_method tags [--save]
    senti_method: the sentiment method to use - "AVERAGE" or "POLAR"
    tags        : comma separated POS tags to make the bags of words used in the
                  linear regression model. Any combination of:
                  adj,adverb,noun,verb,other
    [--save]    : save output to figure file."""

    if len(args) < 2:
        print usage_text.format(prog_name)
        sys.exit(1)

    senti_method = args[0]

    tags = args[1].split(",")

    if len(args) > 2 and args[2] == "--save":
        fig_prefix = prog_name.split('.')[0]
        fig_prefix += "-" + senti_method.lower()
        fig_prefix += "-" + ",".join(tags)
    else:
        fig_prefix = None

    make_plot(senti_method, tags, fig_prefix)
    print ""
