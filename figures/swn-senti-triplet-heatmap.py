import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import FuncFormatter

import figutils as fu
from sentimodels import datautils as du
from sentimodels import sentiutils as su


def make_fig(fig_name=None):

    train, test, dev = du.load_data()

    # concatenate all sentences in sst
    all_sentences = []
    for d in train:
        all_sentences.append(d['sen_str'])
    for d in test:
        all_sentences.append(d['sen_str'])
    for d in dev:
        all_sentences.append(d['sen_str'])

    vocab = [] # all words/tokens without repeats

    for s in all_sentences:
        tokens = s.split()
        for t in tokens:
            if t.lower() not in vocab:
                vocab.append(t.lower())

    sentiments_average = [] # use 'average' wordwise sentiment method
    sentiments_popular = [] # use 'popular' wordwise sentiment method

    for word in vocab:
        sent_av = su.swn_sentiment(word, "AVERAGE", map_to_pos=False)
        if sent_av is not None:
            sentiments_average.append(sent_av)
        sent_pop = su.swn_sentiment(word, "POPULAR", map_to_pos=False)
        if sent_pop is not None:
            sentiments_popular.append(sent_pop)

    # map to a 2d coordinates with the following parameterisations
    def get_x(pos, obj):
        return 0.5*(pos - neg)

    def get_y(pos, obj):
        return (obj-1) * np.sqrt(0.75) + np.sqrt(0.75)

    xy_av = np.empty((len(sentiments_average), 2))
    for idx, senti in enumerate(sentiments_average):
        pos, neg, obj = senti
        xy_av[idx, :] = (get_x(pos, obj), get_y(pos, obj))

    xy_pop = np.empty((len(sentiments_popular), 2))
    for idx, senti in enumerate(sentiments_popular):
        pos, neg, obj = senti
        xy_pop[idx, :] = (get_x(pos, obj), get_y(pos, obj))
    

    def hexbin_ax_fmt(ax):
        ax.set_xlim([-0.7, 0.7])
        ax.set_ylim([-0.15, 1.02])
        for spine in ax.spines.values():
            spine.set_visible(False)
        ax.tick_params(top='off', bottom='off', left='off', right='off',
                labelleft='off', labelbottom='off')
        ax.annotate('Obj. = 1', xy=(0, 0.93), ha='center')
        ax.annotate('Pos. = 1', xy=(0.5, -0.12), ha='center')
        ax.annotate('Neg. = 1', xy=(-0.5, -0.12), ha='center')
        ax.plot([-0.525,0.525],[-0.02,-0.02], 'k', linewidth=0.5)
        ax.plot([0,0.525],[np.sqrt(0.75)+0.025,-0.02], 'k', linewidth=0.5)
        ax.plot([-0.525,0],[-0.02,np.sqrt(0.75)+0.025], 'k', linewidth=0.5)

    def log_label_fmt(x, pos):
        return '$10^{{{:d}}}$'.format(int(x))

    fig = plt.figure(figsize=(6,2.5))

    av_ax = fig.add_axes([0.02,0.15,0.4,0.8], aspect='equal') # top
    av_hb = av_ax.hexbin(xy_av[:,0], xy_av[:,1], bins='log', mincnt=1,
            gridsize=40, cmap=cm.Reds, vmin=0, vmax=5, linewidths=0)
    av_ax.set_xlabel('Method = "AVERAGE"', labelpad=10)
    hexbin_ax_fmt(av_ax)

    pop_ax = fig.add_axes([0.44,0.15,0.4,0.8], aspect='equal') # top
    pop_hb = pop_ax.hexbin(xy_pop[:,0], xy_pop[:,1], bins='log', mincnt=1,
            gridsize=40, cmap=cm.Reds, vmin=0, vmax=5, linewidths=0)
    pop_ax.set_xlabel('Method = "POPULAR', labelpad=10)
    hexbin_ax_fmt(pop_ax)

    cb_ax = fig.add_axes([0.86, 0.2, 0.03, 0.7])
    cb = fig.colorbar(pop_hb, cax=cb_ax, format=FuncFormatter(log_label_fmt))
    cb.set_label('Counts')

    fu.show_or_save(fig, fig_name)


if __name__ == "__main__":
    prog_name = sys.argv[0]
    args = sys.argv[1:]
    
    if len(args) > 0 and args[0] == "--save":
        fig_name = prog_name.split('.')[0]
    else:
        fig_name = None

    make_fig(fig_name)
