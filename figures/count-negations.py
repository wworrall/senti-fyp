from sentimodels import datautils as du


def count_negations(conllus):

    neg_ctr = 0

    forms = {}
    indices = []
    for idx, conllu in enumerate(conllus):
        for wl in conllu:
            if wl['deprel'] == "neg":
                form = wl['form'].lower()
                if form not in forms:
                    forms[form] = 1
                else:
                    forms[form] += 1
                neg_ctr += 1
                indices.append(idx)

    return neg_ctr, forms, indices

if __name__=="__main__":

    train_data, test_data, dev_data = du.load_data()

    train_sents = [ d['sen_str'] for d in train_data ]
    test_sents = [ d['sen_str'] for d in test_data ]
    dev_sents = [ d['sen_str'] for d in dev_data ]

    # parse both train and test in to UD format
    train_conllu_texts = du.run_parser_conllu(train_sents)
    test_conllu_texts = du.run_parser_conllu(test_sents)
    dev_conllu_texts = du.run_parser_conllu(dev_sents)

    # parse conllus in to dict form
    train_conllus = [ du.parse_conllu_text(ct) for ct in train_conllu_texts ]
    test_conllus = [ du.parse_conllu_text(ct) for ct in test_conllu_texts ]
    dev_conllus = [ du.parse_conllu_text(ct) for ct in dev_conllu_texts ]


    # count negations
    train_negs, train_negs_forms, train_neg_inds = count_negations(train_conllus)
    text = "negations in train data: {} / {}\nforms: {}\nFirst 50 indices: {}\n"
    print text.format(train_negs, len(train_conllus), train_negs_forms,
            train_neg_inds[:50])

    test_negs, test_negs_forms, test_neg_inds = count_negations(test_conllus)
    text = "negations in test data: {} / {}\nforms: {}\nFirst 50 indices: {}\n"
    print text.format(test_negs, len(test_conllus), test_negs_forms,
            test_neg_inds[:50])

    dev_negs, dev_negs_forms, dev_neg_inds = count_negations(dev_conllus)
    text = "negations in dev data: {} / {}\nforms: {}\nFirst 50 indices: {}\n"
    print text.format(dev_negs, len(dev_conllus), dev_negs_forms,
            dev_neg_inds[:50])
