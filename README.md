# Beyond a bag of words: sentence level sentiment analysis #

Sentiment analysis final year project at University of Bristol. Submitted in support of the degree of Master of Engineering in Engineering Mathematics.

Supervisor: Prof. Jonathon Lawry

Co-supervisor Prof. Nello Cristiani

### What does this repository include? ###
This repository contains the source for the work described in the project report. It also includes the latex source code to generate the report itself.

### How do I get set up? ###

* Download this repository and ensure the software dependencies (below) are installed.

* Download the Stanford Sentiment Treebank (http://nlp.stanford.edu/~socherr/stanfordSentimentTreebank.zip) and unzip it so that the 'stanfordSentimentTreebank' directory is inside the 'figures' directory.

* Download the Stanford CoreNLP package (http://nlp.stanford.edu/software/stanford-corenlp-full-2016-10-31.zip) and unzip it so that the 'stanford-corenlp-full-2016-10-31' directory is inside the 'figures' directory.

* To run all of the models in all of the modes described in the report and generate results and figures navigate to 'figures' directory and run 'make'. Nb. this will take several hours to complete.

* To also make the report navigate to the 'report' directory and run 'make'. Nb. This may require some non-default latex packages.

### Dependencies ###

Software

* Python 2.7 including:
    - SentiWordNet (NLTK library)
    - Matplotlib
    - NumPy
    - SciPy
* Java 8
* Latex suite
* GNU make
* Stanford CoreNLP package (tested only with 2016-10-31 release)

Data

* Stanford Sentiment Treebank

### Who do I talk to? ###

* Owner of this repo - William Worrall